<html>
<head>
<style>
li:hover {
    background-color: #eee;
}
</style>
</head>
<body>
<?php
require_once('config.php');
$keywords = $cs_params['keywords'];

if(!empty($keywords)){
    echo "<h2>Keywords</h2>";
    echo "<ol>";
    foreach($keywords as $keyword){
        echo "<li>".$keyword.": <a target=\"_blank\" href=\"content-check.php?keyword=".urlencode($keyword)."\">Check Posts</a> | <a target=\"_blank\" href=\"meta-check.php?keyword=".urlencode($keyword)."\">Check Metas</a> | <a target=\"_blank\" href=\"others-check.php?keyword=".urlencode($keyword)."\">Check Others</a></li>";
    }
    echo "</ol>";
}
?>
</body>
</html>