<?php
function highlightStr($haystack, $needle1, $needle2, $highlightColorValue) {
    // return $haystack if there is no highlight color or strings given, nothing to do.
   if (strlen($highlightColorValue) < 1 || strlen($haystack) < 1 || strlen($needle1) < 1 || strlen($needle2) < 1) {
       return $haystack;
   }
   preg_match_all("/$needle1+|$needle2+/i", $haystack, $matches);
   if (is_array($matches[0]) && count($matches[0]) >= 1) {
       foreach ($matches[0] as $match) {
           $haystack = str_replace($match, '<span style="font-weight:bold;background-color:'.$highlightColorValue.';">'.$match.'</span>', $haystack);
       }
   }
   return $haystack;
}

function searchSnippet($text, $query1, $query2)
{
    //words
    //$words = join('|', explode(' ', preg_quote($query)));
    $words = join('|', array($query1, $query2));

    //lookahead/behind assertions ensures cut between words
    $s = '\s\x00-/:-@\[-`{-~'; //character set for start/end of words
    preg_match_all('#(?<=['.$s.']).{1,30}(('.$words.').{1,30})+(?=['.$s.'])#uis', $text, $matches, PREG_SET_ORDER);

    //delimiter between occurences
    $results = array();
    foreach($matches as $line) {
        $results[] = htmlspecialchars($line[0], 0, 'UTF-8');
    }
    $result = join(' <b>(...)</b> ', $results);

    //highlight
    $result = preg_replace('#'.$words.'#iu', "<span style=\"font-weight:bold;background-color:yellow;\">\$0</span>", $result);

    if($result){
        return "<b>(...)</b> ".$result." <b>(...)</b> ";
    }
}

$keyword_1 = 'Office of Hearing Services';
$keyword_2 = 'OHS';

require $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
global $wpdb;
$checks = $wpdb->get_results( 
    "SELECT ID, post_title, post_content, post_excerpt FROM  wp_posts where post_status = 'publish' and (post_title like '%".$keyword_1."%' or post_content like '%".$keyword_1."%' or post_excerpt like '%".$keyword_1."%' or post_title like '%".$keyword_2."%' or post_content like '%".$keyword_2."%' or post_excerpt like '%".$keyword_2."%') ORDER BY `wp_posts`.`ID` ASC"
);

$i = 1;
$export = [];

foreach($checks as $check){
    $snippet = null;
    $source = home_url().'/wp-admin/post.php?post='.$check->ID.'&action=edit';
    $preview = get_permalink($check->ID);
    $snippet_title = highlightStr($check->post_title, $keyword_1, $keyword_2, 'yellow');
    $snippet_excerpt = highlightStr($check->post_excerpt, $keyword_1, $keyword_2, 'yellow');
    $content = strip_tags($check->post_content);
    $content_highlighted = highlightStr($content, $keyword_1, $keyword_2, 'yellow');
    $snippet_content = searchSnippet($content, $keyword_1, $keyword_2);

    // Output results on screen
    echo '#'.$i.'<br><br>';
    //echo '<b>ID:</b> <span style="font-weight:bold;background-color:#FFA500;color:white;"> &nbsp;'.$check->ID.'&nbsp; </span><br>';
    echo '<b>Source (Backend):</b> <a target="_blank" href="' . $source . '">' . $source . '</a><br>';
    echo '<b>Preview:</b> <a target="_blank" href="' . $preview . '">' . $preview . '</a><br><br>';
    echo '<b>Title:</b> ' . $snippet_title . '<br><br>';
    echo '<b>Excerpt:</b> ' . $snippet_excerpt . '<br><br>';
    echo '<b>Content:</b> '.$content_highlighted.'<br><br>';
    echo '<b>Snippet:</b> '.$snippet_content.'<br><br>';
    echo '<hr>';

    // For export to CSV
    $export_item = [];
    $export_item['snippet'] = "Title: " . strip_tags($snippet_title) . "\n\nExcerpt: " . strip_tags($snippet_excerpt) . "\n\nContent: " . strip_tags($snippet_content);
    $export_item['source'] = $source;
    $export_item['preview'] = $preview;
    $export[] = $export_item;

    $i++;
}

$file = fopen("content-check2.csv","w");

foreach ($export as $fields) {
    fputcsv($file, $fields);
}

fclose($file);