<?php
/*
 * Place this file at dev site.
 */

require $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';

//$post_id = $_GET['post'];
$post_ids = $_GET['posts'];
$post_ids_temp = explode(',',$post_ids);
$filter_ids = [];
foreach($post_ids_temp as $pid){
    $filter_ids[] = (int)($pid);
}

global $wpdb;

$check_dev = $wpdb->get_results( 
    "SELECT ID, post_title, post_content, post_excerpt, post_name FROM wp_posts where ID in (".implode(',',$filter_ids).") ORDER BY `wp_posts`.`ID` ASC"
);

wp_send_json($check_dev);