<?php
function highlightStr($haystack, $needles = [], $highlightColorValue) {
    // return $haystack if there is no highlight color or strings given, nothing to do.
   if (strlen($highlightColorValue) < 1 || strlen($haystack) < 1 || strlen($needle1) < 1 || strlen($needle2) < 1) {
       return $haystack;
   }
   $needle1 = $needles[0];
   $needle2 = $needles[1];
   $needle3 = $needles[2];
   $needle4 = $needles[3];
   preg_match_all("/$needle1+|$needle2+|$needle3+|$needle4+/i", $haystack, $matches);
   if (is_array($matches[0]) && count($matches[0]) >= 1) {
       foreach ($matches[0] as $match) {
           $haystack = str_replace($match, '<span style="font-weight:bold;background-color:'.$highlightColorValue.';">'.$match.'</span>', $haystack);
       }
   }
   return $haystack;
}

function searchSnippet($text, $queries = [])
{
    //words
    $words = join('|', array($queries));

    //lookahead/behind assertions ensures cut between words
    $s = '\s\x00-/:-@\[-`{-~'; //character set for start/end of words
    preg_match_all('#(?<=['.$s.']).{1,30}(('.$words.').{1,30})+(?=['.$s.'])#uis', $text, $matches, PREG_SET_ORDER);

    //delimiter between occurences
    $results = array();
    foreach($matches as $line) {
        $results[] = htmlspecialchars($line[0], 0, 'UTF-8');
    }
    $result = join(' <b>(...)</b> ', $results);

    //highlight
    $result = preg_replace('#'.$words.'#iu', "<span style=\"font-weight:bold;background-color:yellow;\">\$0</span>", $result);

    if($result){
        return "<b>(...)</b> ".$result." <b>(...)</b> ";
    }
}

$keyword_1 = 'contactus@audioclinic.com.au';
$keyword_2 = 'customer.care@digitalhearing.com.au';
$keyword_3 = 'contactus@hearinglife.com.au';
$keyword_4 = 'customer.care@hearinglife.com.au';
$keywords = [$keyword_1, $keyword_2, $keyword_3, $keyword_4];

require $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
global $wpdb;
$checks = $wpdb->get_results( 
    "SELECT id, title, display_meta, notifications FROM wp_rg_form LEFT JOIN wp_rg_form_meta ON `wp_rg_form`.`id` = `wp_rg_form_meta`.`form_id` where 
    display_meta like '%".$keyword_1."%' or display_meta like '%".$keyword_2."%' or display_meta like '%".$keyword_3."%' or display_meta like '%".$keyword_4."%' or 
    notifications like '%".$keyword_1."%' or notifications like '%".$keyword_2."%' or notifications like '%".$keyword_3."%' or notifications like '%".$keyword_4."%'  
    ORDER BY `wp_rg_form`.`id` ASC"
);

$i = 1;
$export = [];

if($checks){
    foreach($checks as $check){
        
        $snippet = null;
        $preview_url = array();
        $preview_link = array();
        
        $content_dm = $check->display_meta;
        $content_dm_highlighted = highlightStr($content_dm, $keywords, 'yellow');
        $snippet_dm_content = searchSnippet($content_dm, $keywords);
        if(!$snippet_dm_content){
            $snippet_dm_content = $content_dm_highlighted;
        }
        
        $content_n = $check->notifications;
        $content_n_highlighted = highlightStr($content_n, $keywords, 'yellow');
        $snippet_n_content = searchSnippet($content_n, $keywords);
        if(!$snippet_n_content){
            $snippet_n_content = $content_n_highlighted;
        }

        // Output results on screen
        echo '#'.$i.'<br><br>';
        echo '<b>Form ID:</b> '.$check->id.'<br><br>';
        echo '<b>Form Title:</b> '.$check->title.'<br><br>';
        //echo '<b>Fields:</b> ' . $snippet_dm_content . '<br><br>';
        echo '<b>Notifications:</b> ' . $snippet_n_content . '<br><br>';
        echo '<hr>';

        // For export to CSV
        $export_item = [];
        $export_item['Form ID'] = $check->id;
        $export_item['Form Title'] = $check->title;
        //$export_item['Fields'] = $snippet_dm_content;
        $export_item['Notifications'] = $snippet_n_content;
        $export[] = $export_item;

        $i++;
    }
}else{
    echo "Keyword(s) not found.";
}

$file = fopen("content-check-gf.csv","w");

foreach ($export as $fields) {
    fputcsv($file, $fields);
}

fclose($file);