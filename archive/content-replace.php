<?php
/*
 * Place this file at live site.
 * This file is to replace live site content with dev site content.
 */

require $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';

// action
if($_GET['action'] == 'liverun'){
    $action_live = true;
    echo '<h3>Live run</h3><hr><br>';
}else{
    $action_live = false;
    echo '<h3>Dry run</h3><hr><br>';
}

// counters
$i = 1;
$i_matched = 0;
$i_content = 0;
$i_excerpt = 0;
$i_meta = 0;
$i_content_updated = 0;
$i_excerpt_updated = 0;
$i_meta_updated = 0;

// setup dev and live site url
$dev_siteurl = "http://digitalhearing2018.oskyweb.com/";
$live_siteurl = "https://digitalhearing.com.au/";

// preparing to query db
global $wpdb;

// preparing read spreadsheet required files
require('spreadsheet-reader/php-excel-reader/excel_reader2.php');
require('spreadsheet-reader/SpreadsheetReader.php');

// the file
$spreasheet = 'docs/ADHS_Free_Content_Analysis.xlsx';

// read the file
$reader = new SpreadsheetReader($spreasheet);

// preparing an array to store the matched ids
$matched_ids = [];

// scanning the spreadsheet records
foreach ($reader as $row)
{
    if(strtolower($row[4]) == 'done'){
        // get url
        $url = $row[1];
        
        // get id
        $querystring = parse_url($url, PHP_URL_QUERY);
        parse_str($querystring, $vars);
        $matched_ids[] = $vars['post'];
    }
}

if($matched_ids){
    // find record from dev site base on the matched ids
    $read_src_url = $dev_siteurl . 'api-test/content-check/get-post.php?posts='.implode(',',$matched_ids);
    $response = wp_remote_request( 
        $read_src_url,
        array(
          'method' => 'GET',
          'timeout' => 45,
          'headers' => array(
            'Authorization' => 'Basic ' . base64_encode( 'osky:interactive' )
          )
        )
    );
    echo '<em>(reading '.$read_src_url.')</em><br><hr>';
    if( is_wp_error($response) ){
        $output = "Error reading source.";
        echo '<br><em>(['.date("Y-m-d H:i:s").']'.$output.'<br>['.date("Y-m-d H:i:s").'] Attempting to get posts '.implode(',',$matched_ids).'. '.var_export($response, TRUE).')</em><br>';
    }
    $check_dev_list = json_decode( wp_remote_retrieve_body( $response ) );

    if(!$check_dev_list){
        exit;
    }

    foreach ($check_dev_list as $check_dev) {

        $rid = $check_dev->ID;

        // find record from live site base on the id found
        $check_live = $wpdb->get_results( 
            "SELECT ID, post_title, post_content, post_excerpt, post_name FROM  wp_posts where ID = ".$rid." ORDER BY `wp_posts`.`ID` ASC"
        );

        // Output results on screen
        echo '#'.$i.'<br><br>';
        echo '<b>Excel - Post ID:</b> ' . $rid . '<br><br>';
        echo '<b>Dev - Title:</b> ' . $check_dev->post_title . '<br>';
        echo '<b>Live - Title:</b> ' . $check_live[0]->post_title . '<br>';
        echo '<b>Dev - Slug:</b> ' . $check_dev->post_name . '<br>';
        echo '<b>Live - Slug:</b> ' . $check_live[0]->post_name . '<br>';

        if($check_dev->post_title == $check_live[0]->post_title && $check_dev->post_name == $check_live[0]->post_name){
            echo '<br><b style="background-color:yellow;">MATCHED</b><br><br>';
            $i_matched++;
            
            echo 'Replacing post content and excerpt of the following (from live site, '.$live_siteurl.' ):<br><br>';
            echo '<textarea rows="10" cols="100" readonly="readonly">'.$check_live[0]->post_content.'</textarea><br><br>';
            echo '<textarea rows="5" cols="100" readonly="readonly">'.$check_live[0]->post_excerpt.'</textarea><br><br>';
            echo 'with (from dev site, '.$dev_siteurl.' ):<br><br>';
            echo '<textarea rows="10" cols="100" readonly="readonly">'.$check_dev->post_content.'</textarea><br><br>';
            echo '<textarea rows="5" cols="100" readonly="readonly">'.$check_dev->post_excerpt.'</textarea><br><br>';
            
            if(!empty($check_live[0]->post_content) && !empty($check_dev->post_content)){
                $i_content++; // increase counter

                if($action_live === true){
                    // update live
                    $result_update_live = $wpdb->update('wp_posts', array(
                        'post_content' 	=> $check_dev->post_content,
                        'post_modified'	=> date('Y-m-d H:i:s', time()),
                        'post_modified_gmt'	=> date('Y-m-d H:i:s', time())
                    ), array('ID' 		=> $rid));
                    // output result
                    if($result_update_live){
                        echo '<span style="background-color:green;color:white;">'.$result_update_live.' row(s) of post content updated. Post ID: '.$rid.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                        $i_content_updated++; // increase counter
                    }else if($result_update_live === 0){
                        echo '<span style="background-color:#eee;">Content matched. No post content updated for Post ID: '.$rid.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                    }else if($result_update_live === false){
                        echo '<span style="background-color:red;color:white;">Failed to update post content for Post ID: '.$rid.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                    }else{
                        echo '<span style="background-color:red;color:white;">Other errors. Failed to update post content for Post ID: '.$rid.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                    }
                }
            }

            if(!empty($check_live[0]->post_excerpt) && !empty($check_dev->post_excerpt)){
                $i_excerpt++; // increase counter

                if($action_live === true){
                    // update live
                    $result_update_live = $wpdb->update('wp_posts', array(
                        'post_excerpt' 	=> $check_dev->post_excerpt,
                        'post_modified'	=> date('Y-m-d H:i:s', time()),
                        'post_modified_gmt'	=> date('Y-m-d H:i:s', time())
                    ), array('ID' 		=> $rid));
                    // output result
                    if($result_update_live){
                        echo '<span style="background-color:green;color:white;">'.$result_update_live.' row(s) of post excerpt updated. Post ID: '.$rid.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                        $i_excerpt_updated++; // increase counter
                    }else if($result_update_live === 0){
                        echo '<span style="background-color:#eee;">Excerpt matched. No post excerpt updated for Post ID: '.$rid.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                    }else if($result_update_live === false){
                        echo '<span style="background-color:red;color:white;">Failed to update post excerpt for Post ID: '.$rid.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                    }else{
                        echo '<span style="background-color:red;color:white;">Other errors. Failed to update post excerpt for Post ID: '.$rid.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                    }
                }
            }

            echo 'Replacing the following post meta for this post on live site: '.$live_siteurl.'.<br>';
            $check_postmeta_live = $wpdb->get_results( 
                "SELECT post_id, meta_key, meta_value FROM wp_postmeta where post_id = ".$rid." and meta_value like '%free%'"
            );
            $check_postmeta_live_mk = []; //store the list of meta_key
            if($check_postmeta_live){
                echo '<br>';
                foreach($check_postmeta_live as $check){
                    // skip if it's not publish post
                    if(get_post_status($check->post_id) != 'publish'){
                        continue;
                    }
                    // display result
                    echo '- <b>meta key:</b> '.$check->meta_key.'<br>&nbsp;&nbsp;<b>meta value:</b><br>';
                    echo '&nbsp;&nbsp;<textarea rows="1" cols="100" readonly="readonly">'.$check->meta_value.'</textarea><br>';
                    //store the meta_key
                    $check_postmeta_live_mk[] = $check->meta_key;
                    $i_meta++; // increase counter
                }
            }else{
                echo '- N/A<br>';
            }
            echo '<br>';
            echo 'with the following post meta from this post on dev site: '.$dev_siteurl.'.<br>';
            
            // find record from dev site base on the id found
            $read_src_url = $dev_siteurl . 'api-test/content-check/get-meta.php?post='.$rid.'&key='.implode(',',$check_postmeta_live_mk);
            $response = wp_remote_request( 
                $read_src_url,
                array(
                    'method' => 'GET',
                    'timeout' => 45,
                    'headers' => array(
                        'Authorization' => 'Basic ' . base64_encode( 'osky:interactive' )
                    )
                )
            );
            echo '<br><em>(reading '.$read_src_url.')</em><br><br>';
            if( is_wp_error($response) ){
                $output = "Error reading source.";
                echo '<br><em>(['.date("Y-m-d H:i:s").']'.$output.'<br>['.date("Y-m-d H:i:s").'] Attempting to get post '.$rid.'. '.var_export($response, TRUE).')</em><br>';
            }
            $check_postmeta_dev = json_decode( wp_remote_retrieve_body( $response ) );

            if($check_postmeta_dev){
                echo '<br>';
                foreach($check_postmeta_dev as $check){
                    // skip if it's not publish post
                    if(get_post_status($check->post_id) != 'publish'){
                        continue;
                    }
                    // display result
                    echo '- <b>meta key:</b> '.$check->meta_key.'<br>&nbsp;&nbsp;<b>meta value:</b><br>';
                    echo '&nbsp;&nbsp;<textarea rows="1" cols="100" readonly="readonly">'.$check->meta_value.'</textarea><br>';

                    if($action_live === true){
                        // update live
                        $result_update_live = $wpdb->update('wp_postmeta', 
                            array(
                                'meta_value' 	=> $check->meta_value
                            ), 
                            array(
                                'post_id' 		=> $rid,
                                'meta_key'      => $check->meta_key
                            )
                        );
                        // output result
                        if($result_update_live){
                            echo '&nbsp;&nbsp;<span style="background-color:green;color:white;">'.$result_update_live.' of post meta row(s) updated. Post ID: '.$rid.', meta key: '.$check->meta_key.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                            $i_meta_updated++; // increase counter
                        }else if($result_update_live === 0){
                            echo '&nbsp;&nbsp;<span style="background-color:#eee;">Content matched. No post meta updated for Post ID: '.$rid.', meta key: '.$check->meta_key.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                        }else if($result_update_live === false){
                            echo '&nbsp;&nbsp;<span style="background-color:red;color:white;">Failed to update post meta for Post ID: '.$rid.', meta key: '.$check->meta_key.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                        }else{
                            echo '&nbsp;&nbsp;<span style="background-color:red;color:white;">Other errors. Failed to update post meta for Post ID: '.$rid.', meta key: '.$check->meta_key.', timestamp: '.date('Y-m-d H:i:s', time()).'</span><br><br>';
                        }
                    }
                }
            }else{
                echo '- N/A<br>';
            }
        }else{
            echo '<br><b style="background-color:red;color:white;">NOT MATCHED</b><br>';
        }

        echo '<br><hr><br>';
        $i++;
    }
}

echo '<h3>Summary</h3>';
echo 'Total records read from spreadsheet: '.($i-1).'<br>';
echo 'Total matched records (which will be updated):'.$i_matched.'<br><br>';
echo 'Total post content records to be updated:'.$i_content.'<br>';
echo 'Total post excerpt records to be updated:'.$i_excerpt.'<br>';
echo 'Total post meta records to be updated:'.$i_meta.'<br><br>';

if($action_live === true){
    echo 'Total post content updated:'.$i_content_updated.'<br>';
    echo 'Total post excerpt updated:'.$i_excerpt_updated.'<br>';
    echo 'Total post meta updated:'.$i_meta_updated.'<br>';
}else{
    echo 'Proceed to live run? <a href="?action=liverun" target="_blank">Yes</a>';
}