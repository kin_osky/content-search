<?php
/*
 * Place this file at dev site.
 */

require $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';

$post_id = (int)$_GET['post'];
$meta_key = $_GET['key'];
$meta_key_temp = explode(',',$meta_key);
$filter_key = [];
foreach($meta_key_temp as $mkey){
    $filter_key[] = "'".esc_sql($mkey)."'";
}

global $wpdb;

$check_dev = $wpdb->get_results( 
    "SELECT post_id, meta_key, meta_value FROM wp_postmeta where post_id = ".$post_id." and meta_key in (".implode(',',$filter_key).")"
);

wp_send_json($check_dev);