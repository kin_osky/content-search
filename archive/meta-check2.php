<?php
function highlightStr($haystack, $needle1, $needle2, $highlightColorValue) {
    // return $haystack if there is no highlight color or strings given, nothing to do.
   if (strlen($highlightColorValue) < 1 || strlen($haystack) < 1 || strlen($needle1) < 1 || strlen($needle2) < 1) {
       return $haystack;
   }
   preg_match_all("/$needle1+|$needle2+/i", $haystack, $matches);
   if (is_array($matches[0]) && count($matches[0]) >= 1) {
       foreach ($matches[0] as $match) {
           $haystack = str_replace($match, '<span style="font-weight:bold;background-color:'.$highlightColorValue.';">'.$match.'</span>', $haystack);
       }
   }
   return $haystack;
}

function searchSnippet($text, $query1, $query2)
{
    //words
    //$words = join('|', explode(' ', preg_quote($query)));
    $words = join('|', array($query1, $query2));

    //lookahead/behind assertions ensures cut between words
    $s = '\s\x00-/:-@\[-`{-~'; //character set for start/end of words
    preg_match_all('#(?<=['.$s.']).{1,30}(('.$words.').{1,30})+(?=['.$s.'])#uis', $text, $matches, PREG_SET_ORDER);

    //delimiter between occurences
    $results = array();
    foreach($matches as $line) {
        $results[] = htmlspecialchars($line[0], 0, 'UTF-8');
    }
    $result = join(' <b>(...)</b> ', $results);

    //highlight
    $result = preg_replace('#'.$words.'#iu', "<span style=\"font-weight:bold;background-color:yellow;\">\$0</span>", $result);

    if($result){
        return "<b>(...)</b> ".$result." <b>(...)</b> ";
    }
}

$keyword_1 = 'Office of Hearing Services';
$keyword_2 = 'OHS';

require $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
global $wpdb;
$checks = $wpdb->get_results( 
    "SELECT post_id, meta_key, meta_value FROM  wp_postmeta where meta_value like '%".$keyword_1."%' or meta_value like '%".$keyword_2."%' ORDER BY `wp_postmeta`.`post_id` ASC"
);

$i = 1;
$export = [];

if($checks){
    foreach($checks as $check){
        
        // skip if it's not publish post
        if(get_post_status($check->post_id) != 'publish'){
            continue;
        }
        
        $snippet = null;
        $preview_url = array();
        $preview_link = array();
        
        $meta_key = $check->meta_key;
        switch($meta_key){
            case '_links_to': $meta_key = 'Page links to'; break;
            case '_yoast_wpseo_title': $meta_key = 'Yoast SEO: SEO Title'; break;
            case '_yoast_wpseo_metadesc': $meta_key = 'Yoast SEO: Meta description'; break;
            case '_wp_attached_file': 
            case '_wp_attachment_backup_sizes': 
            case '_wp_attachment_image_alt':
            case '_wp_attachment_metadata':
                $meta_key = 'Attachments'; break;
            default:
                $meta_key = str_replace('_', ' ', $meta_key);
                $meta_key = preg_replace('/\d/', '>', $meta_key);
                break;    
        }

        $post_type = get_post_type($check->post_id);
        switch($post_type){
            case 'wpcf7_contact_form':
                $source = admin_url('admin.php?page=wpcf7&post='.$check->post_id.'&action=edit');
                $preview = null;
                // find content posts or pages that using this form
                $form_checks = $wpdb->get_results( 
                    "SELECT ID, post_title, post_content, post_excerpt FROM  wp_posts where post_status = 'publish' and post_content like '%contact-form-7 id=\"".$check->post_id."\"%' ORDER BY `wp_posts`.`ID` ASC"
                );
                foreach($form_checks as $form_check){
                    $form_check_permalink = get_permalink($form_check->post_id);
                    $preview_url[] = $form_check_permalink;
                    $preview_link[] = '<a target="_blank" href="' . $form_check_permalink . '">' . $form_check_permalink . '</a>';
                }
                break;
            case 'nav_menu_item':
                $source = admin_url('nav-menus.php');
                break;
            default: 
                $source = get_edit_post_link($check->post_id, '&');
                $check_permalink = get_permalink($check->post_id);
                $preview_url[] = $check_permalink;
                $preview_link[] = '<a target="_blank" href="' . $check_permalink . '">' . $check_permalink . '</a>';
                break;
        }

        $snippet_title = str_replace('&#038;', '&', get_the_title($check->post_id));
        $content = strip_tags($check->meta_value);
        $content_highlighted = highlightStr($content, $keyword_1, $keyword_2, 'yellow');
        $snippet_content = searchSnippet($content, $keyword_1, $keyword_2);
        if(!$snippet_content){
            $snippet_content = $content_highlighted;
        }

        // Output results on screen
        echo '#'.$i.'<br><br>';
        //echo '<b>ID:</b> <span style="font-weight:bold;background-color:#FFA500;color:white;"> &nbsp;'.$check->ID.'&nbsp; </span><br>';
        echo '<b>Field:</b> ' . $meta_key . '<br>';
        echo '<b>Source (Backend):</b> <a target="_blank" href="' . $source . '">' . $source . '</a><br>';
        echo '<b>Preview:</b> ' . implode('<br>', $preview_link) . '<br><br>';
        echo '<b>Title:</b> ' . $snippet_title . '<br><br>';
        echo '<b>Content:</b> '.$content_highlighted.'<br><br>';
        echo '<b>Snippet:</b> '.$snippet_content.'<br><br>';
        echo '<hr>';

        // For export to CSV
        $export_item = [];
        $export_item['snippet'] = "Title: " . strip_tags($snippet_title) . "\n\nField: " . strip_tags($meta_key) . "\n\nContent: " . strip_tags($snippet_content);
        $export_item['source'] = $source;
        $export_item['preview'] = implode('\n', $preview_url);
        $export[] = $export_item;

        $i++;
    }
}else{
    echo "Keyword(s) not found.";
}

$file = fopen("meta-check2.csv","w");

foreach ($export as $fields) {
    fputcsv($file, $fields);
}

fclose($file);