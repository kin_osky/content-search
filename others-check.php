<?php
function highlightStr($haystack, $needle, $highlightColorValue) {
    // return $haystack if there is no highlight color or strings given, nothing to do.
   if (strlen($highlightColorValue) < 1 || strlen($haystack) < 1 || strlen($needle) < 1) {
       return $haystack;
   }
   preg_match_all("/$needle+/i", $haystack, $matches);
   if (is_array($matches[0]) && count($matches[0]) >= 1) {
       foreach ($matches[0] as $match) {
           $haystack = str_replace($match, '<span style="font-weight:bold;background-color:'.$highlightColorValue.';">'.$match.'</span>', $haystack);
       }
   }
   return $haystack;
}

function searchSnippet($text, $query)
{
    //words
    $words = join('|', explode(' ', preg_quote($query)));

    //lookahead/behind assertions ensures cut between words
    $s = '\s\x00-/:-@\[-`{-~'; //character set for start/end of words
    preg_match_all('#(?<=['.$s.']).{1,30}(('.$words.').{1,30})+(?=['.$s.'])#uis', $text, $matches, PREG_SET_ORDER);

    //delimiter between occurences
    $results = array();
    foreach($matches as $line) {
        $results[] = htmlspecialchars($line[0], 0, 'UTF-8');
    }
    $result = join(' <b>(...)</b> ', $results);

    //highlight
    $result = preg_replace('#'.$words.'#iu', "<span style=\"font-weight:bold;background-color:yellow;\">\$0</span>", $result);

    if($result){
        return "<b>(...)</b> ".$result." <b>(...)</b> ";
    }
}

require_once('config.php');
$site_code = $cs_params['site_code'];
$keywords = $cs_params['keywords'];
$table_prefix = $cs_params['table_prefix'];

$keyword = $_GET['keyword'];
echo "Searching for keyword: ".$keyword."<hr>";
// Check whether keyword that passed through is in our list
if(!in_array($keyword, $keywords)){
    echo "Error.";
    exit;
}

require $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
global $wpdb;

/* Search through wp_options */
$search_table = 'option';
echo "<h2>Searching ".$search_table." table.</h2>";

$checks = $wpdb->get_results( 
    "SELECT option_id, option_name, option_value FROM ".$table_prefix."options where option_value like '%".$keyword."%' ORDER BY `".$table_prefix."options`.`option_id` ASC"
);

$i = 1;
$export = [];

foreach($checks as $check){
    
    $snippet = null;
    $option_name = $check->option_name;
    
    $content = strip_tags($check->option_value);
    $content_highlighted = highlightStr($content, $keyword, 'yellow');
    $snippet_content = searchSnippet($content, $keyword);
    if(!$snippet_content){
        $snippet_content = $content_highlighted;
    }

    // Output results on screen
    echo '#'.$i.'<br><br>';
    echo '<b>Field:</b> ' . $option_name . '<br>';
    echo '<b>Content:</b> '.$content_highlighted.'<br><br>';
    echo '<b>Snippet:</b> '.$snippet_content.'<br><br>';
    echo '<hr>';

    // For export to CSV
    $export_item = [];
    $export_item['field'] = strip_tags($option_name);
    $export_item['content'] = strip_tags($snippet_content);
    $export[] = $export_item;

    $i++;
}

if(!$checks){
    echo "No results found.";
}

$file = fopen("export/others-check-".$search_table."-".$site_code."-".$keyword.".csv","w");
foreach ($export as $fields) {
    fputcsv($file, $fields);
}
fclose($file);

/* Search through wp_rg_form_meta */
$search_table = 'rg_form_meta';
echo "<h2>Searching ".$search_table." table.</h2>";

$checks = $wpdb->get_results( 
    "SELECT form_id, display_meta, entries_grid_meta, confirmations, notifications FROM ".$table_prefix."rg_form_meta where display_meta like '%".$keyword."%' or entries_grid_meta like '%".$keyword."%' or confirmations like '%".$keyword."%' or notifications like '%".$keyword."%' ORDER BY `".$table_prefix."rg_form_meta`.`form_id` ASC"
);

$i = 1;
$export = [];

foreach($checks as $check){
    
    // convert into object, to retrive fields
    $display_meta = json_decode($check->display_meta);
    $notifications = json_decode($check->notifications);
    $notifications = reset($notifications);
    $noti_name = $notifications->name;
    $noti_email_to = $notifications->to;
    $noti_email_bcc = $notifications->bcc;
    $noti_email_from = $notifications->from;
    
    // Output results on screen
    echo '#'.$i.'<br><br>';
    echo '<b>Form ID:</b> ' . $check->form_id . '<br>';
    echo '<b>Form Title:</b> ' . $display_meta->title . '<br>';
    echo '<b>Notification Title:</b> ' . $noti_name . '<br>';
    echo '<b>Notification Email To:</b> ' . $noti_email_to . '<br>';
    echo '<b>Notification Email BCC:</b> ' . $noti_email_bcc . '<br>';
    echo '<b>Notification Email From:</b> ' . $noti_email_from . '<br>';
    echo '<br>';
    echo '<b>Display Meta:</b> ' . highlightStr(strip_tags($check->display_meta), $keyword, 'yellow') . '<br>';
    echo '<b>Entries Grid Meta:</b> ' . highlightStr(strip_tags($check->entries_grid_meta), $keyword, 'yellow') . '<br>';
    echo '<b>Confirmations:</b> ' . highlightStr(strip_tags($check->confirmations), $keyword, 'yellow') . '<br>';
    echo '<b>Notifications:</b> ' . highlightStr(strip_tags($check->notifications), $keyword, 'yellow') . '<br>';
    echo '<hr>';

    // For export to CSV
    $export_item = [];
    $export_item['form_id'] = $check->form_id;
    $export_item['form_title'] = $display_meta->title;
    $export_item['notification_title'] = $noti_name;
    $export_item['notification_email_to'] = $noti_email_to;
    $export_item['notification_email_bcc'] = $noti_email_bcc;
    $export_item['notification_email_from'] = $noti_email_from;
    //$export_item['display_meta'] = strip_tags(searchSnippet(strip_tags($check->display_meta), $keyword));
    //$export_item['entries_grid_meta'] = strip_tags(searchSnippet(strip_tags($check->entries_grid_meta), $keyword));
    //$export_item['confirmations'] = strip_tags(searchSnippet(strip_tags($check->confirmations), $keyword));
    //$export_item['notifications'] = strip_tags(searchSnippet(strip_tags($check->notifications), $keyword));
    $export[] = $export_item;

    $i++;
}

if(!$checks){
    echo "No results found.";
}

$file = fopen("export/others-check-".$search_table."-".$site_code."-".$keyword.".csv","w");
foreach ($export as $fields) {
    fputcsv($file, $fields);
}
fclose($file);

/* Search through wp_options */
$search_table = 'um';
echo "<h2>Searching ".$search_table." table.</h2>";

$checks = $wpdb->get_results( 
    "SELECT meta_key, meta_value FROM ".$table_prefix."usermeta where meta_value like '%".$keyword."%' ORDER BY `".$table_prefix."usermeta`.`umeta_id` ASC"
);

$i = 1;
$export = [];

foreach($checks as $check){
    
    $meta_key = $check->meta_key;
    
    $content = strip_tags($check->meta_value);
    $content_highlighted = highlightStr($content, $keyword, 'yellow');
    $snippet_content = searchSnippet($content, $keyword);
    if(!$snippet_content){
        $snippet_content = $content_highlighted;
    }

    // Output results on screen
    echo '#'.$i.'<br><br>';
    echo '<b>Field:</b> ' . $meta_key . '<br>';
    echo '<b>Content:</b> '.$content_highlighted.'<br><br>';
    echo '<hr>';

    // For export to CSV
    $export_item = [];
    $export_item['field'] = strip_tags($meta_key);
    $export_item['content'] = strip_tags($snippet_content);
    $export[] = $export_item;

    $i++;
}

if(!$checks){
    echo "No results found.";
}

$file = fopen("export/others-check-".$search_table."-".$site_code."-".$keyword.".csv","w");
foreach ($export as $fields) {
    fputcsv($file, $fields);
}
fclose($file);

/* Search through wp_options */
$search_table = 'u';
echo "<h2>Searching ".$search_table." table.</h2>";

$checks = $wpdb->get_results( 
    "SELECT user_email FROM ".$table_prefix."users where user_email like '%".$keyword."%' ORDER BY `".$table_prefix."users`.`ID` ASC"
);

$i = 1;
$export = [];

foreach($checks as $check){
    
    $content = strip_tags($check->user_email);
    $content_highlighted = highlightStr($content, $keyword, 'yellow');
    $snippet_content = searchSnippet($content, $keyword);
    if(!$snippet_content){
        $snippet_content = $content_highlighted;
    }

    // Output results on screen
    echo '#'.$i.'<br><br>';
    echo '<b>Content:</b> '.$content_highlighted.'<br><br>';
    echo '<hr>';

    // For export to CSV
    $export_item = [];
    $export_item['content'] = strip_tags($snippet_content);
    $export[] = $export_item;

    $i++;
}

if(!$checks){
    echo "No results found.";
}

$file = fopen("export/others-check-".$search_table."-".$site_code."-".$keyword.".csv","w");
foreach ($export as $fields) {
    fputcsv($file, $fields);
}
fclose($file);