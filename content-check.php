<?php
function highlightStr($haystack, $needle, $highlightColorValue) {
    // return $haystack if there is no highlight color or strings given, nothing to do.
   if (strlen($highlightColorValue) < 1 || strlen($haystack) < 1 || strlen($needle) < 1) {
       return $haystack;
   }
   preg_match_all("/$needle+/i", $haystack, $matches);
   if (is_array($matches[0]) && count($matches[0]) >= 1) {
       foreach ($matches[0] as $match) {
           $haystack = str_replace($match, '<span style="font-weight:bold;background-color:'.$highlightColorValue.';">'.$match.'</span>', $haystack);
       }
   }
   return $haystack;
}

function searchSnippet($text, $query)
{
    //words
    $words = join('|', explode(' ', preg_quote($query)));

    //lookahead/behind assertions ensures cut between words
    $s = '\s\x00-/:-@\[-`{-~'; //character set for start/end of words
    preg_match_all('#(?<=['.$s.']).{1,30}(('.$words.').{1,30})+(?=['.$s.'])#uis', $text, $matches, PREG_SET_ORDER);

    //delimiter between occurences
    $results = array();
    foreach($matches as $line) {
        $results[] = htmlspecialchars($line[0], 0, 'UTF-8');
    }
    $result = join(' <b>(...)</b> ', $results);

    //highlight
    $result = preg_replace('#'.$words.'#iu', "<span style=\"font-weight:bold;background-color:yellow;\">\$0</span>", $result);

    if($result){
        return "<b>(...)</b> ".$result." <b>(...)</b> ";
    }
}

require_once('config.php');
$site_code = $cs_params['site_code'];
$keywords = $cs_params['keywords'];
$table_prefix = $cs_params['table_prefix'];

$keyword = $_GET['keyword'];
echo "Searching for keyword: ".$keyword."<hr>";
// Check whether keyword that passed through is in our list
if(!in_array($keyword, $keywords)){
    echo "Error.";
    exit;
}

require $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
global $wpdb;
$checks = $wpdb->get_results( 
    "SELECT ID, post_title, post_content, post_excerpt FROM  ".$table_prefix."posts where post_status = 'publish' and (post_title like '%".$keyword."%' or post_content like '%".$keyword."%' or post_excerpt like '%".$keyword."%') ORDER BY `".$table_prefix."posts`.`ID` ASC"
);

$i = 1;
$export = [];

foreach($checks as $check){
    $snippet = null;
    $source = home_url().'/wp-admin/post.php?post='.$check->ID.'&action=edit';
    $preview = get_permalink($check->ID);
    $snippet_title = highlightStr($check->post_title, $keyword, 'yellow');
    $snippet_excerpt = highlightStr($check->post_excerpt, $keyword, 'yellow');
    $content = strip_tags($check->post_content);
    $content_highlighted = highlightStr($content, $keyword, 'yellow');
    $snippet_content = searchSnippet($content, $keyword);

    // Output results on screen
    echo '#'.$i.'<br><br>';
    //echo '<b>ID:</b> <span style="font-weight:bold;background-color:#FFA500;color:white;"> &nbsp;'.$check->ID.'&nbsp; </span><br>';
    echo '<b>Source (Backend):</b> <a target="_blank" href="' . $source . '">' . $source . '</a><br>';
    echo '<b>Preview:</b> <a target="_blank" href="' . $preview . '">' . $preview . '</a><br><br>';
    echo '<b>Title:</b> ' . $snippet_title . '<br><br>';
    echo '<b>Excerpt:</b> ' . $snippet_excerpt . '<br><br>';
    echo '<b>Content:</b> '.$content_highlighted.'<br><br>';
    echo '<b>Snippet:</b> '.$snippet_content.'<br><br>';
    echo '<hr>';

    // For export to CSV
    $export_item = [];
    $export_item['snippet'] = "Title: " . strip_tags($snippet_title) . "\n\nExcerpt: " . strip_tags($snippet_excerpt) . "\n\nContent: " . strip_tags($snippet_content);
    $export_item['source'] = $source;
    $export_item['preview'] = $preview;
    $export[] = $export_item;

    $i++;
}

if(!$checks){
    echo "No results found.";
}

$file = fopen("export/content-check-".$site_code."-".$keyword.".csv","w");

foreach ($export as $fields) {
    fputcsv($file, $fields);
}

fclose($file);